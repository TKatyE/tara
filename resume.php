<?php

  $ini_array = parse_ini_file("config.ini");
  $root_url = $ini_array["root_url"];
  
  include "head.php";

?>
  <?php
    include "nav.php";
  ?>
	<div class="col-lg-12 title-header">
    <h1>Resume</h1>
	</div>
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h5><a href="TaraSokolowski.pdf" target="_blank">Download as a PDF</a></h5>
        <h2>Key Skills and Interests</h2>
			</div>
			<div class="col-12">
					<p>Entrepreneurial analyst with a background in scientific research, software and web development and the business and nonprofit start-up process. Excellent written and verbal communicator able to translate customer requirements into technical solutions. Strong ability to break 'big picture' ideas down into logistics and requirements while maintaining integrity of overall goal.</p>
			</div>
			<div class="col-12">
				<div class="row">
				<div class="col-sm-4">
  				<p>Web design and development<br />
 					 Machine learning and data analysis<br />
  				 Database design and management<br /></p>
				</div>
				<div class="col-sm-4">
					<p>
  				Project management and coordination<br />
  				Scientific writing and communication<br />
  				Bioinformatics, biochemistry and cell biology research<br /></p>
				</div>
				<div class="col-sm-4">
					<p>
 				 Business strategy and market research<br />
  			 Technical product management<br />
  			 Research grant writing and applications</p>
				</div>
			</div>

  <h2>Work Experience</h2>

  <h3>Managing Director/Board Member (Secretary)</h3>

  <h4>Donors Cure Foundation (April 2013 - Current)</h4>

  <p>Leading the start-up process for the Donors Cure Foundation (<a href="https://www.donorscure.org">https://www.donorscure.org/</a>), a new, nonprofit organization that allows members of the general public to fund specific biomedical research projects led by researchers at universities/institutions across the US. Has included:</p>
  <ul>
	
  <li><p>Conducting extensive market research about the current state of science research funding, crowdfunding, social media marketing and nonprofit fundraising to develop business, strategic, marketing and financial plans for the initial three years of operation.</p></li>
  <li><p>Developing business operations workflows.</p></li>
  <li><p>Working closely with the technical development teams to design a scalable web platform with a public-facing site and extensive administrative capabilities that meets operational goals and requirements; assisting with front and backend development.</p></li>
  <li><p>Actively engaging with potential donors, senior level directors at universities/institutions and researchers to share the mission and strategy of Donors Cure and encourage participation.</p></li>
  <li><p>Working closely with Marketing Director to develop marketing strategies.</p></li>
  <li><p>Designing associated materials tailored to specific audiences.</p></li>
  <li><p>Planning and implementing promotional events geared at different audiences.</p></li>
  <li><p>Recruiting researchers to post projects, reviewing proposal submissions, editing researchers' project descriptions and preparing projects to 'go live' on DonorsCure.org.</p></li>
  <li><p>Developing training guides for effective science communication strategies and working with researchers to translate complex scientific ideas to lay audiences.</p></li>
	
  </ul>

  <h3>Post-Doctoral Scholar/Program Coordinator</h3>

  <h4>Medical University of South Carolina, Center for Biomedical Imaging &amp; Radiology Department (March 2013 - Current)</h4>

  <ul>
  <li><p>Headed organizational committee for the 2nd Annual Charleston Conference on Alzheimer&#8217;s Disease (http://www.charlestonconferences.org/); Managed all aspects of event coordination including scheduling, travel arrangements, budgeting and application processing; Developed a Rails-based web platform for online application submission and review.</p></li>
  <li><p>Developed the &#8216;Protocol Tracking System&#8217;, an online application to manage clinical trial protocol submission adn management of studies within the Radiology Department.</p></li>
  <li><p>Developed the &#8216;Radiology Grant Tracker&#8217;, an online application to keep a current database of funded, unfunded, planned and submitted grants and grant applications for faculty members within the Radiology Department.</p></li>
  <li><p>Prepared the FY2013 Center for Biomedical Imaging Annual Report.</p></li>
  <li><p>Managing the Center for Biomedical Imaging and Radiology Department websites using SiteExecutive.</p></li>
  <li><p>Coordinating the &#8216;Kurtosis Imaging Network (KIN)&#8217; Project, an MRI image repository for researchers using DKI to share data and collaborate.</p></li>
  </ul>

  <h3>PhD Research Student in Bioinformatics</h3>

  <h4>University of Dundee, Scotland, UK (November 2010 - Current)</h4>

  <ul>
  <li><p>Thesis: Updates to the PIPs Predictor of Human Protein-Protein Interactions
  Took over maintenance and development of a large program previously written by the group; Involved self-learning of the background material, the algorithm and a new programming language.</p></li>
  <li><p>Identified key areas for improvement and targeted further development to capitalize on these niches; Wrote a new version of PIPs with a different underlying methodology not yet implemented in the field to increase the effectiveness of the tool.</p></li>
  <li><p>Improved public access to PIPs by redesigning the web server to make it more up-to-date, user-friendly and visually appealing (to be available soon at http://www.compbio.dundee.ac.uk/www-pips; currently old version).</p></li>
  <li><p>Collaborated with two other groups within the College of Life Sciences to practically implement the tool to aid their experimental design processes; Presented summary and fine details of findings in clear and effective webpages.</p></li>
  <li><p>After identifying a hole in the field, manually searched the current literature for all available information on snoRNAs from Arabidopsis thaliana and assembled a MySQL database of centralized information and wrote a user-friendly web front-end for other research groups to access the material (to be available soon at http://www.compbio.dundee.ac.uk/www-sno).</p></li>
  <li><p>Assisted with lab practicals for first, second and third year undergraduate students; Explained complex subjects to students and ensured lessons and assignments were completed accurately and on time.</p></li>
  <li><p>Prepared and presented multiple research documents and presentations describing work undertaken, tailored to a range of audiences including group members, inter-disciplinary groups and the general public.</p></li>
  </ul>

  <h3>Intern, Medical University of South Carolina, Office of Industry Partnerships July 2010-Sept 2010</h3>

  <ul>
  <li><p>During an eight-week summer internship working for the OIP, assembled a catalogue of information detailing current research at the University and identifying products with potential for industrial collaboration.</p></li>
  <li><p>Created a portfolio of current pipelines and future interests of the top 50 pharmaceutical companies and top 20 medical device companies that could be used by the OIP to match the interests of MUSC with outside industry.</p></li>
  <li><p>Assisted with miscellaneous administrative tasks including redesigning brochures, converting paper forms to digital formats and updating the SalesForce contact database.</p></li>
  <li><p>Worked part-time in the MUSC Writing Center helping proofread and edit medical students’ essays and personal statements.</p></li>
  </ul>

  <h3>Research Student, University of St Andrews, Scotland, UK June 2009-May 2010</h3>

  <ul>
  <li><p>BBSRC-funded summer studentship in the MFW lab researching the DNA/RNA binding kinetics of the archaebacterial SSB protein from Sulfolobus solfataricus; wrote a detailed report and prepared a poster describing the methods employed and results attained from the work.</p></li>
  <li><p>Five-month research project for the RME lab creating a mutant of the Bunyamwera Bunyavirus lacking the NSs protein; prepared a presentation and 7,000-word dissertation detailing the methods and results of the work.</p></li>
  </ul>

  <h3>Teaching Assistant, Fast-Paced High School Biology, Johns Hopkins CTY, NY June 2008-Aug 2008</h3>

  <ul>
  <li>Assisted in teaching two, three-week sessions of high school biology to 24 13&#8211;16 year olds with the John Hopkins Center for Talented Youth at Siena College in Loudonville, NY.</li>
  <li>Taught several classes and designed and ran practical sessions for the students to reinforce daily topics, offered emotional and academic support to students and planned for and participated in parent-teacher conferences at the end of each session.</li>
  </ul>

  <h2>Education</h2>

  <p>University of Dundee, Dundee, Scotland, UK Nov 2010-April 2013, Ph.D. in Bioinformatics</p>

  <p>University of St Andrews, St Andrews, Scotland, UK Sept 2006 - June 2010, BSc (2.1 Honours) Cell Biology
  - Deans List, 2009-2010</p>

  <p>The Hill School, Pottstown, PA, USA Sept 2002 - May 2006, High School degree (3.9 GPA)
  - Graduated summa cum laude; recipient of the Phi Beta Kappa Association of Philadelphia Prize for excellence in scholastic achievement and honor and integrity
  Peer-elected member of the Honor Council for two years, Captain of the Girls’ Winter and Spring Track teams and Prefect</p>

  <h2>Skills and Interests</h2>

  <h3>Technical Skills</h3>
	<p>Proficient in a wide variety of computer skills including:</p>
	<ul>
   <li>Windows and Unix operating systems</li>
   <li>Graphic design and photo editing (Adobe Photoshop, Adobe Illustrator and Aperture)</li>
   <li>Scripting languages (Python and Java)</li>
   <li>MySQL and SQLite database design and management</li>
   <li>Ruby on Rails, Javascript, JQuery and HTML and CSS</li>
   <li>Version management with Subversion and Git/Github</li>
  </ul>

  <h3>Science communication</h3>

  <ul>
	  <li><p>Google CS First Assistant for Girls Coding Club at Oakland Elementary School, Charleston, SC.</p></li>
  <li><p>Former member of STEM Scotland, the Society for the promotion of Science, Technology, Engineering and Mathematics outreach and Researchers in Residence, a scheme placing researchers in local schools to engage students in science.</p></li>
  <li><p>Participated in the Dundee Sensation Science Centre's Create and Inspire training course - designed, prepared and ran a two-day exhibit at the Centre with an activity explaining DNA transcription and translation to children of all ages.</p></li>
  <li><p>Divisional representative for the University of Dundee College of Life Sciences Open Doors Day (2011) - helped organize a large event aimed at making science research accessible to the general public.</p></li>
  </ul>

  <h3>Web Design</h3>

  <ul>
  <li><p>Designed and developed multiple websites ranging from static sites for small businesses (<a href="http://www.get-intrigued.com/">http://www.get-intrigued.com/</a> and <a href="http://www.weddingsandwhatknot.com/">http://www.weddingsandwhatknot.com/</a>), Ruby on Rails applications (<a href="http://app.charlestonconferences.org/">http://app.charlestonconferences.org</a>, among others not currently publically available), PHP/MySQL sites (PIPs: Predictor of Protein-Protein Interactions, currently not online) as well as other small, working sites for internal projects or personal use.</p></li>
  </ul>

  <h3>Extracurricular Activities and Other Interests</h3>

  <ul>
  <li><p>Long distance running; raced the Edinburgh Marathon in 2009 and raised over $1000 for Breast Cancer Care UK.</p></li>
  <li><p>Other athletics; former member of the University of St Andrews Shinty team and other extensive athletic experience in skiing, field hockey, track and field and golf.</p></li>
  <li><p>Former volunteer with Families First Charity, St Andrews, as a Befriender; met bi-weekly with a 10-year-old boy with a dysfunctional family situation to offer him emotional support and some time away.</p></li>
  </ul>
      </div>
    </div>
  </div>
  
  <?php include "logo.php" ?>
  <?php include "footer.php" ?>  