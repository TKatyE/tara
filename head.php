<?php
  $ini_array = parse_ini_file("config.ini");
  $root_url = $ini_array["root_url"];
?>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<title>Tara Eckenrode Sokolowski, Ph.D.</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href=<?php echo $root_url . "/bootstrap/css/bootstrap.min.css"?> rel="stylesheet" media="screen">
	<link href=<?php echo $root_url . "/bootstrap/css/bootstrap.css"?> rel="stylesheet" media="screen">
	<link href=<?php echo $root_url . "/bootstrap/css/overrides2.css"?> rel="stylesheet" media="screen">
	
  <link href='http://fonts.googleapis.com/css?family=Satisfy' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Quicksand' rel='stylesheet' type='text/css'>
  

</head>