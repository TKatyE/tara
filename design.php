<?php

  $ini_array = parse_ini_file("config.ini");
  $root_url = $ini_array["root_url"];
  
  include "head.php";

?>
  <?php
    include "nav.php";
  ?>
	<div class="col-lg-12 title-header">
    <h1>Web & Design Samples</h1>
	</div>
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <h2>Websites</h2>
        <h4><a href="https://www.donorscure.org/" target="_blank">Donors Cure</a></h4>
				<p>Initial site designed/developed outside; took over complete maintenance and further development of site in 05/2014.</p>
        <p>Contributed to design of site.</p>
				<p>Added enhanced functionality (PHP/CodeIgniter, MySQL, Javascript, JQuery, AJAX)</p>
        <h4><a href="http://www.get-intrigued.com/" target="_blank">Intrigue Design and Events</a></h4>
        <p>Designed logo and Wordpress blog.</p>
        <p>Designed and developed full site.</p>
        <h4><a href="http://www.weddingsandwhatknot.com/" target="_blank">Weddings & What Knot</a></h4>
        <p>Designed logo and Wordpress blog.</p>
        <p>Designed and developed full site.</p>
        <h4><a href="http://app.charlestonconferences.org" target="_blank">Charleston Conference on Alzheimer's Disease Application System</a></h4>
        <p>Designed and developed Ruby on Rails system for to manage CCAD applications. Allows applicants to submit their information, administrators to view all applications, download the information as a spreadsheet or individual PDFs and record decisions and reviewers to rank their set of applications to review.</p>
			</div>
			<div class="col-lg-6">
				
				
        <h2>Design and Print</h2>
				<h4><a href="DonorsCureInformationSummary.pdf" target="_blank">Donors Cure Information</a></h4>
					<p>Graphical business plan providing an overview of Donors Cure for general audiences. Designed and developed all content.</p>
				<h4><a href="DCInformationFlier.pdf" target="_blank">Donors Cure Information Booklet</a></h4>
					<p>Eight-page booklet describing Donors Cure. Designed and developed all content.</p>
				<h4><a href="baseballs.png" target="_blank">Donors Cure Baseball Handout</a></h4>
				<p>Designed as handout for sponsored baseball game.</p>
				<h4><a href="ResearchPostcard.pdf" target="_blank">Donors Cure Researcher Flier</a></h4>
					<p>One-page flier to explain Donors Cure to researchers. Designed and developed all content.</p>
				<h4><a href="ResearchersPacket.pdf" target="_blank">Donors Cure Researcher Information</a></h4>
					<p>Packet of information about Donors Cure for researchers. Designed and developed all content.</p>
				<h4><a href="friendreferpostcard.pdf" target="_blank">Donors Cure Referral Postcard</a></h4>
					<p>Refer a Friend postcard for Donors Cure. Designed and developed all content.</p>
        <h4><a href="CCAD2013brochure.pdf" target="_blank">CCAD 2013 Brochure</a></h4>
        <p>Print booklet recapping the 2013 CCAD.</p>
      </div>
	</div>
  </div>

  
<?php include "logo.php" ?>
<?php include "footer.php" ?>