<?php
  $ini_array = parse_ini_file("config.ini");
  $root_url = $ini_array["root_url"];
  
?>
<body>
	<div id="bg-color">
  <div class="container">
    <div class="row">
    <div class="col-sm-6 col-lg-12 col-sm-12">
      <ul class="nav nav-pills border_btm" style="margin-top: 60px;">
        <li class="logo">Tara Eckenrode Sokolowski, Ph.D.</li>
        <li class="button-style"><a href="index.php">resume</a></li>  
        <li class="button-style"><a href="design.php">web & design</a></li>
        <li class="button-style"><a href="writing.php">writing</a></li>
				
      </ul>
      <p style="text-align: center; color: #fff; font-size: 16px; font-family: 'Quicksand';">(e) <a href="mailto:tara@eckenrode.com" style="color: #fff;">tara@eckenrode.com</a> (p) 610-246-6832 (in) <a href="https://www.linkedin.com/profile/public-profile-settings?trk=prof-edit-edit-public_profile" style="color: #fff;">LinkedIn</a></p>
    </div>
    </div>
  </div>
</div>
