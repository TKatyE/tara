<?php

  $ini_array = parse_ini_file("config.ini");
  $root_url = $ini_array["root_url"];
  
  include "head.php";

?>
  <?php
    include "nav.php";
  ?>
	<div class="col-lg-12 title-header">
    <h1>Writing</h1>
	</div>
  
  <div class="container">
    <div class="row">
      <div class="col-xs-6 col-lg-12 col-sm-12">
        <p>To download a variety of writing samples, right-click on each link and click 'Download As'.</p>
        <h2>Blog Posts</h2>
        <h4><a href="Sequestration.pdf">The Coming Impact of the Sequestration</a></h4>
        <p>Post about the impact of the Sequestration on NIH funding and science research.</p>
        <h4><a href="Running-TaraEckenrode.pdf">Running</a></h4>
        <p>A response to those who supported me to run the 2009 Edinburgh Marathon.</p>
        <h4><a href="BabesAndScience.pdf">Babes. And Science.</a></h4>
        <p>Thoughts on the struggle of being a woman in the science profession and my future family life (written during my Ph.D.)</p>
        <h2>Scientific Writing</h2>
        <h4><a href="TaraEckenrode-Salmonella.pdf">Salmonella: Friend or foe?</a></h4>
        <p>A scientific blog post written about a piece of research published using Salmonella to treat melanoma.</p>
        <h4><a href="TaraEckenrodeWriting.pdf">Thesis Excerpts</a></h4>
        <p>Selected excerpts from my Ph.D. Thesis.</p>
        <h4><a href="TaraThesisRevisedFull.pdf">Full Thesis</a></h4>
        <p>Full thesis: Computation Prediction of Protein-Protein Interactions</p>

      </div>
    </div>
  </div>
<?php include "logo.php" ?>
<?php include "footer.php" ?>